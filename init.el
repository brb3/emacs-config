;;; init.el --- My super-duper emacs config
;; Author: Bobby Burden III <bobby@brb3.org>
;; URL: https://gitlab.com/brb3/emacs-config

;;; Commentary:
;; This is my personal Emacs config, inspired heavily by Spacemacs, this config
;; aims to remain light-weight, and require very minimal setup. The only package
;; needed is `use-package`. After that, all other packages are installed
;; automatically.

;;; Code:
(setq delete-old-versions -1
      version-control t
      vc-make-backup-files t
      backup-directory-alist `(("." . "~/.emacs.d/backups"))
      vc-follow-symlinks t
      auto-save-file-name-transforms
      '((".*" "~/.emacs.d/auto-save-list/" t))
      inhibit-startup-screen t
      ring-bell-function 'ignore
      coding-system-for-read 'utf-8
      coding-system-for-write 'utf-8
      sentence-end-double-space nil
      scroll-conservatively 10000
      scroll-preserve-screen-position t
      initial-scratch-message ";; Happy hacking!")
(fset 'yes-or-no-p 'y-or-n-p)
(setq-default fill-column 80)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(line-number-mode -1)
(blink-cursor-mode -1)
(global-auto-revert-mode 1)
(global-display-line-numbers-mode 1)
(column-number-mode 1)
(add-hook 'text-mode-hook 'turn-on-auto-fill)
(server-start)

;; Make the custom-set-variables stuff live in a seperate file
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

;; Setup MELPA
(require 'package)
(add-to-list 'package-archives
	     (cons "melpa" "https://melpa.org/packages/") t)
(package-initialize)

;; Now get use-package going. We'll use this for all other packages
(eval-when-compile
  ;; Following line is not needed if use-package.el is in ~/.emacs.d
  (require 'use-package))
(setq use-package-always-ensure t)

;; Inherit my shell environment (doesn't happen automatically in X)
(use-package exec-path-from-shell
  :init
  (exec-path-from-shell-initialize))

;; Keychain makes SSH and GPG agent management real nice
(use-package keychain-environment
  :init
  (keychain-refresh-environment))

;; auto-package-update will prompt us when it's time to update packages
(use-package auto-package-update
  :init
  (setq auto-package-update-prompt-before-update t
	auto-package-update-delete-old-versions t))

;; Add completion to eshell
(use-package esh-autosuggest
  :hook (eshell-mode . esh-autosuggest-mode))

;; Setup diminish to keep the mode line nice
(use-package diminish
  :diminish auto-revert-mode
  :diminish abbrev-mode
  :diminish eldoc-mode)

;; Moar better pdf support
(use-package pdf-tools
  :demand t
  :config (pdf-tools-install))

;; Let's get EVIL
(use-package evil
  :diminish evil-mode
  :init
  (setq evil-want-integration nil)
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1)
  (evil-set-initial-state 'dired-mode 'emacs)
  (evil-ex-define-cmd "q[uit]" nil)
  (use-package evil-escape
    :diminish evil-escape-mode
    :config
    (setq-default evil-escape-key-sequence "jk")
    (evil-escape-mode 1))
  (use-package evil-org
    :after org
    :config
    (add-hook 'org-mode-hook 'evil-org-mode)
    (add-hook 'evil-org-mode-hook
	      (lambda ()
		(evil-org-set-key-theme
		 '(navigation insert textobjects todo additional calendar))))
    (require 'evil-org-agenda)
    (evil-org-agenda-set-keys))
  (use-package evil-collection
    :config
    (evil-collection-init)))

;; RSS feed support
(use-package elfeed
  :config
  (add-to-list 'evil-motion-state-modes 'elfeed-search-mode)
  (add-to-list 'evil-motion-state-modes 'elfeed-show-mode)

  (evil-define-key* 'motion elfeed-search-mode-map
		    "r"  #'elfeed-search-show-entry
		    "gb" #'elfeed-search-browse-url
		    "gr" #'elfeed-search-update--force
		    "gR" #'elfeed-search-fetch)

  (evil-define-key* 'motion elfeed-show-mode-map
		    "gb" #'elfeed-show-visit
		    "gj" #'elfeed-show-next
		    "gk" #'elfeed-show-prev))
(load-file "~/.emacs.d/feeds.el")

;; Mastodon mode for time wasting :)
(use-package mastodon
  :init
  (load-file "~/.emacs.d/mastodon.el"))

;; Magit is the best git wrapper
(use-package magit
  :config
  (unbind-key "SPC" magit-mode-map)
  (use-package evil-magit)
  (use-package forge))

;; Helpful provides a better *help* buffer
(use-package helpful)

;; Flycheck gives us on-the-fly syntax checking
(use-package flycheck
  :config
  (global-flycheck-mode 1))

;; Always cleanup whitespace in buffers
(use-package whitespace-cleanup-mode
  :hook ((prog-mode text-mode conf-mode) . whitespace-cleanup-mode)
  :diminish whitespace-cleanup-mode)

;; Edit-server for "Edit with Emacs" in Firefox
(use-package edit-server
  :init (edit-server-start))

;; Make this beast look good
(set-frame-font "Iosevka-11")

(use-package doom-themes
  :init
  (defvar doom-themes-enable-bold)
  (defvar doom-themes-enable-italic)
  (setq doom-themes-enable-bold   t
	doom-themes-enable-italic t)
  (load-theme 'doom-nord-light)
  (doom-themes-org-config))

(use-package smart-mode-line
  :init
  (setq sml/theme 'dark)
  (sml/setup))

;; diminish a few modes to cleanup the mode line
(diminish 'auto-revert-mode)
(diminish 'auto-fill-function)

;; Gotta have emojis
(use-package emojify)

;; Use Helm (previously using ivy)
(use-package helm
  :diminish helm-mode
  :init
  (require 'helm-config)
  (setq helm-input-idle-delay 0.01
	helm-split-window-inside-p t
	helm-yas-display-key-on-candidate t
	helm-quick-update t
	helm-M-x-requires-pattern nil
	helm-ff-skip-boring-files t)
  (helm-mode 1))

;; Which-key shows us what key combos are setup as we are typing
(use-package which-key
  :diminish which-key-mode
  :config
  (which-key-mode 1))

;; Org
(setq org-todo-keywords '((type "TODO" "HOLD" "DONE"))
      org-agenda-files '("~/Orgs")
      org-capture-templates
      '(("r" "Reply to Email" entry (file+headline "~/Orgs/journal.org" "Tasks")
	 "** TODO %?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n%a\n")
	("t" "TODO" entry (file+headline "~/Orgs/journal.org" "Tasks")
	 "** TODO %?")
	("j" "Journal" entry (file+olp+datetree "~/Orgs/journal.org")
	 "* Entered on %U\n%?")))

;; Convert org-mode to hugo
(use-package ox-hugo
  :after ox)

;; Org-latex fun
(setq org-latex-with-hyperref nil)
(require 'ox-latex)
(unless (boundp 'org-latex-classes)
  (setq org-latex-classes nil))
(add-to-list 'org-latex-classes
	     '("moderncv"
	       "\\documentclass[11pt,a4paper,sans]\{moderncv\}
\\moderncvstyle{banking}
\\moderncvcolor{blue}
\\usepackage[scale=0.75]{geometry}
\[NO-DEFAULT-PACKAGES]
\[NO-PACKAGES]
\[EXTRA]"
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")))

(defun org-open-journal ()
  "Opens the 'journal.org' file at ~/Orgs/journal.org."
  (interactive)
  (find-file "~/Orgs/journal.org"))

;; Projectile makes organizing projects much easier
(use-package projectile
  :diminish projectile-mode
  :init
  (use-package helm-projectile
    :init
    (helm-projectile-on))
  (projectile-mode 1))

;; Search with ripgrep
(use-package deadgrep
  :config
  (evil-set-initial-state 'deadgrep-mode 'emacs))

;;; Programming Language Support

;; Eglot is a client to LSP servers
(use-package eglot)

;; Devdocs integration
(use-package devdocs)

;; Add Dumb-Jump for hopping around to function definitions
(use-package dumb-jump
  :init
  (dumb-jump-mode))

;; Yasnippets - code snippets and easy templating
(use-package yasnippet
  :init
  (use-package yasnippet-snippets)
  (yas-global-mode 1))

;; Using auto-complete for fancy completion
(use-package auto-complete
  :config
  (ac-config-default))

;; Use editorconfig for per-project style config
(use-package editorconfig
  :diminish editorconfig-mode
  :config
  (editorconfig-mode 1))

;; Dockerfile & Docker-compose modes
(use-package dockerfile-mode)
(use-package docker-compose-mode)

;; PHP Mode
(use-package php-mode)

;; Make it a bit easier to write lisp by highlighting parens
(show-paren-mode t)
(use-package rainbow-delimiters)

;; Trusty rusty yaml
(use-package yaml-mode)

;; Markdown
(use-package markdown-mode)

;; Rustic -- Rust development environment
(use-package rustic)

;; Robe gives us fancy completion for Ruby
(use-package robe)

;; Geben gives us support for XDebug and friends
(use-package geben)

;; Elixir support
(use-package elixir-mode)

;; JavaScript
(use-package js2-mode
  :config
  (use-package ac-js2
    :config
    (add-hook 'js2-mode-hook 'ac-js2-mode)))

;;; Misc
;; Emacs-application-framework
;; https://github.com/manateelazycat/emacs-application-framework
;;(add-to-list 'load-path (expand-file-name "~/.emacs.d/projects/emacs-application-framework"))
;;(require 'eaf)

;; Evil nerd commenter
(use-package evil-nerd-commenter
  :config (evilnc-default-hotkeys))

;; Rest client
(use-package restclient)

;;; KEYS
;; Now here's where we get all Spacemacs-ish
;; 'general' let's us make some fun key commands
  (use-package general
    :config
    (setq which-key-idle-delay 0.1)
    (setq general-override-states '(insert
				    emacs
				    hybrid
				    normal
				    visual
				    motion
				    operator
				    replace))
    (general-override-mode)

    (general-define-key
     :states '(normal visual insert emacs)
     :prefix "SPC"
     :non-normal-prefix "C-/"

     ;; simple command
     "SPC" '(helm-M-x :which-key "M-x")

     ;; Misc
     "k" 'helm-show-kill-ring
     "s" 'helm-occur
     "'" 'eshell

     ;; Applications
     "a" '(:ignore t :which-key "Applications")
     "ad" 'dired
     "am" 'mu4e
     "ae" 'elfeed

     ;; Buffer
     "b" '(:ignore t :which-key "Buffer")
     "bb" 'helm-buffers-list
     "bd" 'kill-this-buffer
     "br" 'rename-buffer

     ;; FlyCheck
     "c" '(:ignore t :which-key "FlyCheck")
     "cl" 'flycheck-list-errors
     "cn" 'flycheck-next-error
     "cp" 'flycheck-previous-error
     "cs" 'flycheck-select-checker

     ;; File
     "f" '(:ignore t :which-key "File")
     "fs" 'save-buffer
     "ff" 'helm-find-files
     "fw" 'whitespace-cleanup
     "fg" 'deadgrep

     ;; Help
     "h" '(:ignore t :which-key "Help")
     "hf" 'helpful-function
     "hm" 'describe-mode
     "hp" 'helpful-at-point
     "hv" 'helpful-variable
     "hd" 'devdocs-search

     ;; Magit
     "g" '(:ignore t :which-key "Magit")
     "gs" 'magit-status

     ;; MPD (via MPDel)
     "m" '(:ignore t :which-key "MPD")
     "mp" 'libmpdel-playback-play-pause
     "mn" 'libmpdel-playback-next
     "mP" 'libmpdel-playback-previous
     "ml" 'mpdel-playlist-open
     "mv" 'mpdel-song-open
     "ms" '(:ignore t :which-key "Search")
     "msa" 'mpdel-nav-search-by-artist
     "msl" 'mpdel-nav-search-by-album
     "mst" 'mpdel-nav-search-by-title

     ;; Org
     "o" '(:ignore t :which-key "Org")
     "oa" 'org-agenda
     "oc" 'org-capture
     "oj" 'org-open-journal
     "ott" 'org-todo
     "oti" 'org-clock-in
     "oto" 'org-clock-out
     "otr" 'org-clock-report

     ;; Projects
     "p" '(:ignore t :which-key "Project")
     "pp" 'helm-projectile-switch-project
     "pf" 'helm-projectile-find-file
     "pn" 'projectile-add-known-project
     "pc" 'projectile-compile-project
     "pg" 'deadgrep

     ;; Quit
     "q" '(:ignore t :which-key "Quit")
     "qq" 'kill-emacs

     ;; Window
     "w" '(:ignore t :which-key "Window")
     "wd" 'evil-window-delete
     "wh" 'evil-window-left
     "wj" 'evil-window-down
     "wk" 'evil-window-up
     "wl" 'evil-window-right
     "w-" 'evil-window-split
     "w/" 'evil-window-vsplit
     "wo" 'evil-window-next))
;;; init.el ends here
